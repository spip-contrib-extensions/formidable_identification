<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'erreur_email_utilise' => 'L\'adresse email est déjà utilisée dans ce site.',
	'erreur_verifier_formulaire' => 'Votre réponse contient au moins une erreur. Veuillez vérifier les données saisies dans le formulaire.',
	'label_formulaire_identification_email' => 'Votre email',
	'label_formulaire_identification_nom' => 'Votre nom',
	'formulaire_identification_label_case' => 'Voulez-vous contribuer de manière non-anonyme ?',
	'message_deja_identifie' => 'Vous êtes déjà identifié sur le site, <a href="@url@">veuillez vous déconnectez</a> pour pouvoir anonymiser votre réponse.',
	'message_erreur_inscription_desactivee' => 'L\'inscription des visiteurs est désactivée sur le site.',
	'message_ok_incription_valider' => 'Merci de votre contribution à la consultation MAEDI 21.',
	'message_ok_inscription_validee' => 'Votre inscription a été validée. Votre soumission est prise en compte.',
	'option_afficher_nom' => 'Activer le champ "nom"',
	'option_afficher_nom_explication' => 'Le champ "email" est obligatoire pour l\'inscription, le champ "nom", quant à lui est facultatif.',
	'option_explication_formulaire_identification_email' => 'Explication du champ email',
	'option_explication_formulaire_identification_nom' => 'Explication du champ nom',
	'option_label_formulaire_identification_email' => 'Label du champ "email"',
	'option_label_formulaire_identification_nom' => 'Label du champ "nom"',
	'saisie_formidable_identification_titre' => 'Proposition d\'identification',
);

?>